import { Dialog, Slide } from "@mui/material";
import { TransitionProps } from "@mui/material/transitions";
import React, { useEffect, useState } from "react";
import { Medias, ResponseData } from "../public/Type/ResponseData";
import VideocamIcon from "@mui/icons-material/Videocam";
import { useAPI } from "./api/api";

interface Props {
  open: boolean;
  handleClose: () => void;
  data: ResponseData;
}

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export const DialogDownload: React.FC<Props> = ({
  open,
  handleClose,
  data,
}) => {
  const api = useAPI();
  const [loading, setLoading] = useState<boolean>(false);
  const downloadVideo = async (fileURL: string, fileName: string) => {
    setLoading(true);
    await fetch(fileURL).then((response) => {
      response.blob().then((blob) => {
        let url = window.URL.createObjectURL(blob);
        let a = document.createElement("a");
        a.href = url;
        a.download = fileName;
        a.click();
      });
    }).catch((e) => {
      console.log(e)
      setLoading(false);
    });
    setLoading(false);
  };
  return (
    <Dialog
      open={open}
      TransitionComponent={Transition}
      keepMounted
      onClose={handleClose}
      aria-describedby="alert-dialog-slide-description"
      maxWidth={false}
      PaperProps={{
        className: "paper-dialog-contain"
      }}
    >
      <div className="dialog-contain">
        <div className="image-video">
          <iframe
            src={data?.thumbnail}
            title={data?.title}
            width="300px"
            height="490px"
          />
        </div>
        <div className="download-button-group">
          {data?.medias &&
            data?.medias.map((value: Medias, index: number) => {
              return (
                <div
                  key={index}
                  className="download-button-dialog"
                  onClick={() =>
                    !loading &&
                    downloadVideo(value.url, "download." + value.extension)
                  }
                  style={{
                    backgroundColor: loading ? "#f3f3f3" : "#2e86c1",
                    cursor: loading ? "not-allowed" : "pointer",
                    color: loading ? "black" : "white",
                  }}
                >
                  {value.quality}
                  <span className="circle-span">
                    {value.extension}
                    <VideocamIcon
                      fontSize="small"
                      style={{ margin: "0px 10px" }}
                    />
                    {value.formattedSize}
                  </span>
                </div>
              );
            })}
        </div>
      </div>
    </Dialog>
  );
};
