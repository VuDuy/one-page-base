import React, { createContext, useState } from "react";

type valueLayout = {
  layout: string,
  handleChangeLayout: (value: string) => void
}

const defaultValue = {
  layout: 'light',
  handleChangeLayout: () => {},
}

export const LayoutContext = createContext<valueLayout>(defaultValue);

const LayoutProvider = React.memo((props) => {
  const [layout, setLayout] = useState<string>('light')
  const handleChangeLayout = (value: string) => {
    setLayout(value)
  }


  const value: valueLayout = {
    layout,
    handleChangeLayout,
  };

  return <LayoutContext.Provider value={value as valueLayout} {...props} />;
});

export default LayoutProvider