import React, { useContext } from "react";
import { LayoutContext } from "./LayoutProvider";

export const Footer: React.FC = () => {
  const {layout} = useContext(LayoutContext)
  return <div className="footer-contain" style={{background: layout === 'light' ? '#ffffff' : '#1a2430', width: '100%'}}>
    <div className="footer-left-text">© 2019 - 2023 (v17) <span style={{color: '#0d6efd'}}>SnapTik</span>. All rights reserved.</div>
    <div className="footer-right-contain">
      <a className="footer-right-text" href="https://snaptik.app/landing/contact" style={{ color: layout === 'light' ? 'black' : 'white'}}>Contact</a>
      <a className="footer-right-text" href="https://snaptik.app/landing/terms-of-service" style={{ color: layout === 'light' ? 'black' : 'white'}}>Terms of Service</a>
      <a className="footer-right-text" href="https://snaptik.app/landing/privacy-policy" style={{ color: layout === 'light' ? 'black' : 'white'}}>Privacy Policy</a>
    </div>
  </div>
}