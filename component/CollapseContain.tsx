import { Collapse } from "@mui/material";
import React, { ReactNode, useContext, useState } from "react";
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import { LayoutContext } from "./LayoutProvider";

interface Props {
  children: ReactNode
  title: string
}

export const CollapseContain: React.FC<Props> = ({children, title}) => {
  const [open, setOpen] = useState<boolean>(false)
  const handleChangeOpen = (value: boolean) => {
    setOpen(value)
  }
  const {layout} = useContext(LayoutContext);
  return <div className="collapse-container" style={{background: layout === 'light' ? '#ffffff' : '#1a2430'}}>
    <div style={{display: 'flex', alignItems: 'center', justifyContent: 'space-between', cursor: 'pointer'}} onClick={() => handleChangeOpen(open === true ? false: true)}>
      <p className="collapse-title" style={{ color: layout === 'light' ? 'black' : 'white'}}>{title}</p>
      {open === true ? <KeyboardArrowUpIcon style={{ color: layout === 'light' ? 'black' : 'white'}} /> : <KeyboardArrowDownIcon style={{ color: layout === 'light' ? 'black' : 'white'}} />}
    </div>
    <Collapse in={open} style={{ color: layout === 'light' ? 'black' : 'white'}}>{children}</Collapse>
  </div>
}