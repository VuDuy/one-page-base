import Image from "next/image";
import React, { useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import { ResponseData } from "../public/Type/ResponseData";
import { useAPI } from "./api/api";
import { DialogDownload } from "./DialogDownload";
import { LayoutContext } from "./LayoutProvider";

export const DownloadContain: React.FC = () => {
  const { layout } = useContext(LayoutContext);
  const { t } = useTranslation();
  const [url, setUrl] = useState<string>("");
  const handleChangeUrl = (value: string) => {
    setUrl(value);
  };
  const getUrlFromClipBroad = async () => {
    const text = await navigator.clipboard.readText();
    setUrl(text);
  };
  const [loading, setLoading] = useState<boolean>(false);
  const [data, setData] = useState<ResponseData>();
  const [open, setOpen] = useState<boolean>(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const api = useAPI();

  const sendUrl = async (url: string) => {
    setLoading(true);
    let res = await api.fetcher(
      "post",
      " https://fpttelecom.com/wp-json/aio-dl/video-data/",
      {
        url: url,
      }
    );
    setLoading(false);
    setData(res);
    handleOpen();
    console.log({ res });
  };

  return (
    <div
      className="download-contain"
      style={{ background: layout === "light" ? "#195fd7" : "#0c1625" }}
    >
      <div className="title-text">{t("download.title")}</div>
      <div className="description-text">{t("download.description")}</div>
      <div className="input-contain">
        <div className="download-input">
          <div className="link-contain">
            <Image
              src="/icon/link.svg"
              alt="Vercel Logo"
              width={20}
              height={20}
            />
          </div>
          <input
            placeholder={t("download.input_place_holder") as string}
            value={url}
            onChange={(e) => handleChangeUrl(e.target.value)}
          />
          {url == "" && (
            <div className="paste-button-contain">
              <div
                className="paste-button"
                onClick={() => getUrlFromClipBroad()}
              >
                <Image
                  src="https://snaptik.app/assets/svg/clipboard.svg"
                  alt="Vercel Logo"
                  width={16}
                  height={16}
                />
                <span>{t("download.paste")}</span>
              </div>
            </div>
          )}
          {url != "" && (
            <div className="clear-button-contain">
              <div className="clear-button" onClick={() => handleChangeUrl("")}>
                X<span>{t("download.clear")}</span>
              </div>
            </div>
          )}
        </div>
        {loading && (
          <div className="download-button">
            <div className="loader"></div>
          </div>
        )}
        {!loading && (
          <div className="download-button" onClick={() => sendUrl(url)}>
            <Image
              src="https://snaptik.app/assets/svg/down.svg"
              alt="Vercel Logo"
              width={24}
              height={24}
            />
            <span>{t("download.download")}</span>
          </div>
        )}
      </div>
      <DialogDownload
        open={open}
        handleClose={handleClose}
        data={data as ResponseData}
      />
    </div>
  );
};
