import { useState, useMemo } from "react";
import useSWR from "swr";
import request from "./request";

function useAPI() {
  const [error, setError] = useState({});
  const [data, setData] = useState();
  const [loading, setLoading] = useState(false);
  const [cancel, setCancel] = useState(() => () => false);
  const fetcher = useMemo(() => {
    let ignore = false;
    setCancel(() => () => (ignore = true));
    return async (method: any, endpoint: string, params: any, config = {}) => {
      setError({});
      setLoading(true);
      const access_token = localStorage.getItem("admin_token")
        ? localStorage.getItem("admin_token")
        : localStorage.getItem("user_token");
      request.defaults.headers.common["Authorization"] =
        "Bearer " + access_token;

      return request({
        url: endpoint,
        method: method,
        [method.toLowerCase() === "get" ? "params" : "data"]:
          typeof params === "string" ? JSON.parse(params) : params,
        ...config,
      })
        .then((response) => {
          setData(response.data);
          return response.data;
        })
        .catch((error) =>
          ignore ? null : Promise.reject(errorResponse(error))
        )
        .finally(() => (ignore ? null : setLoading(false)));
    };
  }, []);

  function errorResponse(error: any) {
    if (error.response) {
      const {
        response: { data, status },
      } = error;
      setError(data.errors);
      return { data, status };
    }
  }

  return { fetcher, data, error, loading, cancel };
}

export { useAPI };
