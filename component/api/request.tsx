import axios from "axios";
import { toast } from "react-toastify";

const errors = {
  SERVER_ERROR: "Serve Error",
  EXCEPTION_ERROR: "Exception Error",
};
// create an axios instance
const service = axios.create({
  baseURL: process.env.REACT_APP_API_URL, // api base_url
  timeout: 30 * 1000, // request timeout
});
// request interceptor
service.interceptors.request.use(
  (config) => {
    // Do something before request is sent
    return config;
  },
  (error) => {
    // Do something with request error
    console.log(error); // for debug
    Promise.reject(error);
  }
);

// response interceptor
service.interceptors.response.use(
  (response) => {
    if (response.data.message && response.data.message !== "ok") {
      toast.success(response.data.message, {
        position: "top-center",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
      });
    }
    return response;
  },

  (error) => {
    // console.log(error.response);
    // for debug
    if (error.response.status === 500) {
      toast.error(error.response.data.message, {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
      });
    } else if (error.response.status === 401 || error.status === 403) {
      toast.error(errors.EXCEPTION_ERROR, {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
      });
    } else if (error.response.status === 404) {
      const mess = error.response.data.message;
      if (mess) {
        toast.error(mess, {
          position: "top-center",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
        });
      }
    } else if (error.response.status === 422) {
      toast.error("Incorrect data format", {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
      });
    } else {
      const mess = error.response.data.message;
      if (mess) {
        toast.error(mess, {
          position: "top-center",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
        });
      }
    }

    return Promise.reject(error);
  }
);

export default service;
