import Image from "next/image";
import React, { useContext } from "react";
import { CollapseContain } from "./CollapseContain";
import { LayoutContext } from "./LayoutProvider";

export const Information: React.FC = () => {
  const {layout} = useContext(LayoutContext);
  return (<div style={{background: layout === 'light' ? '#ffffff' : '#1a2430', width: '100%'}}>
    <div className="information-contain">
    <p className="information-title-text" style={{ color: layout === 'light' ? 'black' : 'white'}}>Download SnapTik Android App</p>
    <p className="information-description-text" style={{ color: layout === 'light' ? 'black' : 'white'}}>I now provide an app for downloading TikTok videos. It is fast, easy, with no watermark and HD quality</p>
    <div style={{width: '100%', alignItems: 'center', justifyContent: 'center', display: 'flex'}}>
      <Image src="https://snaptik.app/assets/svg/googleplay.svg" alt="Vercel Logo" width={165} height={86} />
    </div>
    <p className="information-title-text" style={{ color: layout === 'light' ? 'black' : 'white'}}>Download TikTok videos (Musically) Without Watermark for FREE</p>
    <p className="information-description-text" style={{ color: layout === 'light' ? 'black' : 'white'}}>SnapTik.App is one of the best TikTok Downloader available online to download video tiktok without a watermark. You are not required to install any software on your computer or mobile phone, all that you need is a TikTok video link, and all the processing is done on our side so you can be one click away from downloading videos to your devices.</p>
    <p className="key-feature-text" style={{ color: layout === 'light' ? 'black' : 'white'}}>Key features:</p>
    <ul>
      <li style={{ color: layout === 'light' ? 'black' : 'white'}}>No watermark for better quality, which most of the tools out there can't.</li>
      <li style={{ color: layout === 'light' ? 'black' : 'white'}}>Download TikTok videos, Musically videos on any devices that you want: mobile, PC, or tablet. TikTok only allows users to download videos by its application and downloaded videos contain the watermark.</li>
      <li style={{ color: layout === 'light' ? 'black' : 'white'}}>Download by using your browsers: I want to keep things simple for you. No need to download or install any software. I make an application for this purpose as well but you can only install whenever you like.</li>
      <li style={{ color: layout === 'light' ? 'black' : 'white'}}>It's always free. I only place some ads, which support maintaining our services, and further development.</li>
      <li style={{ color: layout === 'light' ? 'black' : 'white'}}>New SnapTik provides users with the ability to download Tiktok's photo slide show as Mp4 Video format. The images and music in the Tiktok slide show will be automatically merged by SnapTik. In addition, you can also download each image in the slide show to your computer right away.</li>
    </ul>
    <CollapseContain title="How to Download video Tiktok no watermark?"><p>test</p></CollapseContain>
    <CollapseContain title="How to get the TikTok video download link?"><p>test</p></CollapseContain>
    <CollapseContain title="Where are TikTok videos saved after being downloaded?"><p>test</p></CollapseContain>
    <CollapseContain title="Does SnapTik.App store downloaded videos or keep a copy of videos?"><p>test</p></CollapseContain>
    <CollapseContain title="Do I need to install instructions or extensions?"><p>test</p></CollapseContain>
    <CollapseContain title="Do I have to pay to Tiktok Downloader without watermark (Snaptik)?"><p>test</p></CollapseContain>
    <CollapseContain title="Can I use this Tiktok video downloader on my Android phone?"><p>test</p></CollapseContain>
    <CollapseContain title="How do I save tiktok video /download my favorite Tik Tok mp4 videos to my iPhone (IOS)?"><p>test</p></CollapseContain>
    <CollapseContain title="Is there a limit to download Tiktok videos at SnapTik?"><p>test</p></CollapseContain>
    <CollapseContain title="Does Snaptik support downloading multiple videos / Download all videos from a certain tiktok / Htags channel?"><p>test</p></CollapseContain>
    <CollapseContain title="Can I download high resolution TikTok videos at SnapTik?"><p>test</p></CollapseContain>
    <CollapseContain title="Can I edit TikTok videos downloaded at SnapTik?"><p>test</p></CollapseContain>
    <CollapseContain title="Does SnapTik provide tiktok mp3 download solution?"><p>test</p></CollapseContain>
    <p className="note-text" style={{ color: layout === 'light' ? '#212529' : 'white'}}><span style={{fontWeight: 800}}>Note: </span>SnapTik is not a tool of Tiktok, we have no relationship with Tiktok or ByteDance Ltd. We only support Tiktok users to download our videos on Tiktok without logo without any trouble. If you have problems with sites like Tikmate or SSSTiktok, try SnapTik, we are constantly updating to make it easy for users to download tiktok videos. Thank you!</p>
  </div>
    </div>)
}