import React, { useContext } from "react";
import { ChangeLanguage } from "./ChangeLanguage";
import { ChangeLayout } from "./ChangeLayout";
import { LayoutContext } from "./LayoutProvider";

export const Header: React.FC = () => {

  const {layout} = useContext(LayoutContext)
  return <div className="container-header" style={{backgroundColor: layout === 'light' ? 'white' : '#202b39'}}>
    <div className="font-logo">
      <span style={{color: '#0075ff'}}>Snap</span>
      <span style={{color: layout === 'light' ? 'black' : 'white'}}>Tik</span>
    </div>
    <div className="flex-contain">
      <ChangeLayout />
      <ChangeLanguage />
    </div>
  </div>
}