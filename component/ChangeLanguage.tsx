import { Popper } from "@mui/material";
import React, { useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import { LayoutContext } from "./LayoutProvider";

export const ChangeLanguage: React.FC = () => {
  const {i18n} = useTranslation()
  const {layout} = useContext(LayoutContext)
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popper' : undefined;
  return <div>
  <div className="language-text" style={{color: layout === 'light' ? 'black' : 'white', cursor: 'pointer'}} aria-describedby={id} onClick={handleClick}>
    Languages
  </div>
  <Popper id={id} open={open} anchorEl={anchorEl}>
    <div className="language-contain-select">
      <div className="language-item" onClick={(e) => {i18n.changeLanguage('en'); handleClick(e)}}>English</div>
      <div className="language-item" onClick={(e) => {i18n.changeLanguage('vi'); handleClick(e)}}>Việt Nam</div>
    </div>
  </Popper></div>
}