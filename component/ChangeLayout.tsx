import React, { useContext } from "react";
import { LayoutContext } from "./LayoutProvider";
import LightModeIcon from '@mui/icons-material/LightMode';
import NightlightIcon from '@mui/icons-material/Nightlight';

export const ChangeLayout: React.FC = () => {
  const {layout, handleChangeLayout} = useContext(LayoutContext)
  return <div className="contain-layout" style={{background: layout === 'light' ? '#e8e8f1' : "#ffffff33"}}>
    {layout === 'light' && <div className="night-layout" onClick={() => handleChangeLayout('night')}>
      <NightlightIcon fontSize="small" />
    </div>}
    {layout === 'night' && <div className="light-layout" onClick={() => handleChangeLayout('light')}>
      <LightModeIcon fontSize="small" style={{color: 'white'}} />
    </div>}
  </div>
}