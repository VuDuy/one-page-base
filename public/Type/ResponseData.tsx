export type Medias = {
  url: string;
  quality: string;
  extension: string;
  size: number;
  formattedSize: string;
  videoAvailable: boolean;
  audioAvailable: boolean;
  chunked: boolean;
  cached: boolean;
};
export type ResponseData = {
  url: string;
  title: string;
  thumbnail: string;
  duration: string;
  source: string;
  medias: Medias[];
  sid: string;
};
