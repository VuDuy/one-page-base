import '../styles/globals.css'
import '../styles/header.css'
import '../styles/footer.css'
import '../styles/download-contain.css'
import '../styles/information.css'
import '../styles/collapse.css'
import "../styles/dialog-download.css"
import type { AppProps } from 'next/app'

export default function App({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}
